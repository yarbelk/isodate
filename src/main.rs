use crate clap;
use chrono::prelude::*;
use chrono_tz::{TimeZone
use clap::{Arg, App}



fn main() {
    let matches = App::new("ISODate")
        .version("0.1.0")
        .author("James Rivett-Carnac")
        .about("Get ISO dates, with possibly timezone support")
        .arg(Arg::with_name("timezone")
            .short("t")
            .long("timezone")
            .required(false)
            .takes_value(true)
        ).get_matches()
    let tz = matches.value_of("timezone")
    if tz == "" {
        let dt = Local::now();
        println!("{}", dt.format("%G-W%V-%u %T%z").to_string());
        return
    }
}
