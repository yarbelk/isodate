package main

import (
	"fmt"
	"time"
)

func main() {
	now := time.Now()
	year, week := now.ISOWeek()
	day := now.Weekday()
	fmt.Printf("%d-W%02d-%d %02d:%02d:%02d\n", year, week, day, now.Hour(), now.Minute(), now.Second())
}
